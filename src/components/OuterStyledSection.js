import styled from 'styled-components'
import theme from './theme'

const OuterStyledSection = styled.section`
  margin: auto;
  font-family: ${theme.baseFont};
  font-weight: normal;
  width: 100%;
  ${'' /* @media */}
`

export default OuterStyledSection