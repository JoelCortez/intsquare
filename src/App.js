import React, { Component } from 'react';
import { ThemeProvider } from "styled-components";
import theme from "./components/theme";
import { HomePage, Header } from "./components";
import './App.css';

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <Header/>
          <HomePage/>
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
