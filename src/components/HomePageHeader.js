import React, { Component } from 'react'
import InnerStyledSection from './InnerStyledSection'
import OuterStyledSection from './OuterStyledSection'

export class HomePageHeader extends Component {
  render() {
    return (
      <HomePageHeaderStyles className='home-page-header__outer'>
        <InnerStyledSection className='home-page-header__inner'>
          {/* <div className='home-page-header__cover'></div> */}

          <div className='home-page-header__image'>

          </div>
        </InnerStyledSection>
      </HomePageHeaderStyles>
    )
  }
}

const HomePageHeaderStyles = OuterStyledSection.extend`
  height: 300px;
  margin: -70px 0 0 0;
  @media(min-width: ${({theme}) => theme.medium.start}) {
    height: 600px;
  }
  @media(min-width: ${({theme}) => theme.large.start}) {
    heigth: 800px;
  }
  .home-page-header {
    &__inner {
      height: 100%;
      padding: 0px;
      margin: 0;
      width: 100%;
      max-width: 100% !important;
      @media (min-width: ${({theme}) => theme.large.start}) {
        margin: auto;
      }
    }
    
    &__cover {
      z-index: 1;
      width: 100%;
      border: 1px solid red;
      position: absolute;
      background-color: rgba(0,0,0,0.3);
      @media(min-width: ${({ theme }) => theme.medium.start}) {
        height: 600px;
      }
      @media(min-width: ${({ theme }) => theme.large.start}) {
        heigth: 800px;
      }
    }

    &__image {
      background-color: white;
      width: 100%;
      height: 100%;
      background-image: url('/assets/img/header-background.png');
      background-repeat: no-repeat;
      background-size: 100%;
      background-position-x: 0%;
      background-position-y: 80%;
    }
  }
`