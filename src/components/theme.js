let theme = {

  transparent:              'transparent',
  black:                    '#000000',
  white:                    '#ffffff',
  blue:                     '#000D72',
  red:                      '#94323E',

  baseFont: '"Inconsolata", monospace',
  
  small: {
    outer: '320px',
    inner: '320px',
    start: '0px',
    end: ' 640px',
  },
  medium: {
    inner: '640px',
    start: '641px',
    end: '1024px'
  },
  large: {
    before: '1139px',
    inner: '1140px',
    start: '1141px',
    end: '1440px',
  },
  xlarge: {
    outer: '1440px',
    inner: '1140px',
    start: '1441px'
  },
}

export default theme